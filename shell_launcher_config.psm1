﻿# pieces of this have been adapted from https://docs.microsoft.com/en-us/windows-hardware/customize/enterprise/shell-launcher

function Create-ShellLauncherClass
{
    $COMPUTER = "localhost"
    $NAMESPACE = "root\standardcimv2\embedded"

    # Create a handle to the class instance so we can call the static methods.
    try {
        $ShellLauncherClass = [wmiclass]"\\$COMPUTER\${NAMESPACE}:WESL_UserSetting"
        return $ShellLauncherClass
        } catch [Exception] {
        write-host $_.Exception.Message; 
        write-host "Make sure Shell Launcher feature is enabled"
        exit
        }
}

function Get-AdminSID
{
    # Return the well-known security identifier for the local administrators group
    $Admins_SID = "S-1-5-32-544"
    return $Admins_SID
}

function Get-UsernameSID($AccountName) {

    try {
        $NTUserObject = New-Object System.Security.Principal.NTAccount($AccountName)
        $NTUserSID = $NTUserObject.Translate([System.Security.Principal.SecurityIdentifier])

        return $NTUserSID.Value
    }
    catch [Exception] {
        write-host $_.Exception.Message; 
        write-host "Have you specified a valid user on the system?"
    }

}

function Get-CustomShells {

<#
.SYNOPSIS

Returns a list of custom shells that have been defined for users on the local system.

.DESCRIPTION

Get-CustomShells uses the WESL_USerSetting class and Get-WmiObject to output the security identifier and shell executable that is configured for each user that has a custom shell defined.

.NOTES

You must run this function with local administrator rights, and 'Shell Launcher' must be checked in the 'Device Lockdown' category of Windows Features.
#>

    $COMPUTER = "localhost"
    $NAMESPACE = "root\standardcimv2\embedded"
    $ShellLauncherClass = Create-ShellLauncherClass
    $IsShellLauncherEnabled = $ShellLauncherClass.IsEnabled()
    $Shells = Get-WmiObject -namespace $NAMESPACE -computer $COMPUTER -class WESL_UserSetting | Select Sid, Shell, DefaultAction | Format-List -Property *
    If ($IsShellLauncherEnabled.Enabled) {
        Write-Host "`nShell Launcher is currently enabled." -ForegroundColor Green
    }
    Else {
        Write-Host "`nShell Launcher is currently disabled." -ForegroundColor Red
    }
    Write-Host "`nCurrent settings for custom shells:"
    $Shells
}

function Set-CustomShell ([String]$AccountName, [String]$ExecutableName, [Int]$CloseAction) {

<#
.SYNOPSIS

Set a custom shell to be used for an account on a local system

.DESCRIPTION

Set-CustomShell uses the WESL_UserSetting class to specify a custom shell executable for a user account security identifier

.PARAMETER AccountName

The name of an account on a local system

.PARAMETER ExecutableName

The name of an executable that will be used as a custom shell

.PARAMETER CloseAction

Optional action to take if the shell closes.  Options include 0=restart shell; 1=restart device; 2=shutdown device.  Default value is 0.

.EXAMPLE

Set the command prompt as the shell for user 'john'

Set-CustomShell 'john' 'cmd.exe'

.EXAMPLE

Set Mozilla Firefox as the shell for user 'joseph'

Set-CustomShell 'joseph' 'C:\Program Files (x86)\Mozilla Firefox\Firefox.exe'

.EXAMPLE

Set Windows Explorer as the shell for all users in the local 'Administrators' group

Set-CustomShell 'Administrators' 'explorer.exe'

.NOTES

You must run this function with local administrator rights, and 'Shell Launcher' must be checked in the 'Device Lockdown' category of Windows Features.
#>
    If (($AccountName -eq '') -OR ($ExecutableName -eq '')) {
        Write-Host 'Please specify an account and executable path.  Exiting...'
        break
    }
    If (($CloseAction -lt 0) -OR ($CloseAction -gt 2)) {
        Write-Host 'Invalid shell exit action option.  Please refer to documentation.  Exiting...'
        break
    }
    ElseIf (!$CloseAction) {
        $CloseAction = 0
    }
    If (-NOT ($AccountName -eq 'Administrators')) { 
        $AccountSID = Get-UsernameSID $AccountName
        $ShellLauncherClass = Create-ShellLauncherClass
        try {
         $ShellLauncherClass.SetCustomShell($AccountSID, $ExecutableName, ($null), ($null), $CloseAction)
         Write-Host 'Shell set for user SID' $AccountSID
        }
        catch [Exception] {
            write-host $_.Exception.Message;
            write-host 'No custom shell set. Exiting...'
            break
        }
    }
    Else {
        $AccountSID = Get-AdminSID
        $ShellLauncherClass = Create-ShellLauncherClass
        $ShellLauncherClass.SetCustomShell($AccountSID, $ExecutableName)
        Write-Host 'Shell set for local Administrators group.'
    }
}

function Remove-CustomShell ([String] $AccountName) {
<#
.SYNOPSIS

Remove a custom shell that is currently defined for an account on a local system

.DESCRIPTION

Remove-CustomShell uses the WESL_UserSetting class to remove a custom shell executable for a user account security identifier

.PARAMETER AccountName

The name of an account on a local system that has a currently defined custom shell

.EXAMPLE

Remove custom shell for user 'john'

Remove-CustomShell 'john'

.NOTES

You must run this function with local administrator rights, and 'Shell Launcher' must be checked in the 'Device Lockdown' category of Windows Features.
#>
    $ShellLauncherClass = Create-ShellLauncherClass
    $AccountSID = Get-UsernameSID $AccountName
    try {
        $ShellLauncherClass.RemoveCustomShell($AccountSID)
        Write-Host "`nShells removed for user" $AccountName
    }
    catch [Exception] {
        write-host $_.Exception.Message;
        write-host "There may not be any custom shells specified for this account"
    }
}

function Enable-ShellLauncher {
<#
.SYNOPSIS

Enables custom shells for all accounts on a local system.

.DESCRIPTION

Enable-ShellLauncher uses the WESL_UserSetting class to enable custom shell executables for all user accounts

.NOTES

You must run this function with local administrator rights, and 'Shell Launcher' must be checked in the 'Device Lockdown' category of Windows Features.
#>
    $ShellLauncherClass = Create-ShellLauncherClass
    $ShellLauncherClass.SetEnabled($TRUE)
    $IsShellLauncherEnabled = $ShellLauncherClass.IsEnabled()
    "`nShell Launcher is set to " + $IsShellLauncherEnabled.Enabled
}

function Disable-ShellLauncher {
<#
.SYNOPSIS

Disables custom shells for all accounts on a local system.

.DESCRIPTION

Disable-ShellLauncher uses the WESL_UserSetting class to disable custom shell executables for all user accounts

.NOTES

You must run this function with local administrator rights, and 'Shell Launcher' must be checked in the 'Device Lockdown' category of Windows Features.
#>
    $ShellLauncherClass = Create-ShellLauncherClass
    $ShellLauncherClass.SetEnabled($FALSE)
    $IsShellLauncherEnabled = $ShellLauncherClass.IsEnabled()
    "`nShell Launcher is set to " + $IsShellLauncherEnabled.Enabled
}