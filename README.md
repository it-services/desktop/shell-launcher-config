# Shell Launcher Configurator

This is a PowerShell module with various cmdlets that helps an admistrator configure the Shell Launcher feature of Windows 10.

Shell Launcher allows an administrator to replace the default Windows Explorer shell with some other application (i.e. a web browser).

Refer to Microsoft documentation available here: https://docs.microsoft.com/en-us/windows-hardware/customize/enterprise/shell-launcher

## Getting Started

The following cmdlets are available with this module:

Get-Customshells returns a list of defined shells per user or group

Set-CustomShell to set a custom shell to be used for an account

Remove-CustomShell to remove a shell currently defined for an account

Enable-ShellLauncher to enable the Windows Shell Launcher

Disable-ShellLauncher to disable the Windows Shell Launcher

For more information, use Get-Help with one of the above cmdlets.

## Prerequisites

The Shell Launcher windows feature must be enabled to use this PowerShell module.

## Installing

Import this module into PowerShell using Import-Module, or make it a part of your PowerShell profile.

## Acknowledgements

Various pieces of this module include bits from the script provided here:

https://docs.microsoft.com/en-us/windows-hardware/customize/enterprise/shell-launcher
